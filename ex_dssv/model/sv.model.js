function SinhVien(_ten, _ma, _matKhau, _email, _toan, _ly, _hoa) {
  this.ten = _ten;
  this.ma = _ma;
  this.email = _email;
  this.matKhau = _matKhau;
  this.toan = _toan;
  this.ly = _ly;
  this.hoa = _hoa;
  this.tinhDTB = function () {
    return (this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3;
  };
}
