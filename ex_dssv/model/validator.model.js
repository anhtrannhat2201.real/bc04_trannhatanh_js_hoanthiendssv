//kiểm tra rỗng bằng object
var validator = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemtraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemtraDiem: function (value, idError, message, min, max) {
    if (value < min || value > max) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemtraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
      if(re.test(value)){
        document.getElementById(idError).innerText = "";
        return true;
      }else{
        document.getElementById(idError).innerText = message
        return false;
      }
  },
   kiemTraId:function(n,value,idError,message) {

    if(n.value= "value") {
        document.getElementById(idError).innerText = "";
        return true;
      }
    else{
    document.getElementById(idError).innerText =message
    return false;
  }
}
};