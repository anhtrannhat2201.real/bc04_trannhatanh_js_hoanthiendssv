//hàm lấy thông tin từ form
function layThongTinTuForm() {
  //   console.log("yes form");
  const maSV = document.getElementById("txtMaSV").value;
  const tenSV = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;
  var sv = new SinhVien(tenSV, maSV, matKhau, email, diemToan, diemLy, diemHoa);
  return sv;
}
//render array dssv ra giao diện
function renderDSSV(svArr) {
  // #tbodySinhVien
  //tạo chuỗi chứa các thẻ <tr></tr>
  var contentHTML = "";
  for (i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    //trContent thẻ tr trong mỗi lần lặp
    var trContent = `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.tinhDTB()}</td>
    <td>
    <button onclick="xoaSinhVien('${
      sv.ma
    }')" class="btn btn-danger">xóa</button>
    <button onclick="suaSinhVien('${
      sv.ma
    }')" class="btn btn-warning">sửa</button>
    </td>
    </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
//hàm tìm kiếm vị trí
function timKiemViTri(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ma == id) {
      return index;
    }
  }
  //không tìm thấy thì trả về -1
  return -1;
}

function showThongTinLenForm(sv) {
  //   console.log("yes form");
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtMaSV").disabled = true;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}
