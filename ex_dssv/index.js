// const DSSV_LOCALSTORAGE = "DSSV";

//tạo 1 mảng trống
var dssv = [];
// var dssv2 = [];
//lấy thông tin từ localStorage
var dssvJson = localStorage.getItem("DSSV");
//xét điều kiện nếu lấy được sẽ thực hiện lệnh
if (dssvJson != null) {
  //lấy JSON.parse(dssvJson) gán cho mảng dssv
  dssv = JSON.parse(dssvJson);
  //array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    //map lại dòng hiện tại cho new SinhVien
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  //render dssv đã lưu vào localStorage ra ngoài output
  renderDSSV(dssv);
}
//chức năng thêm sinh viên
document.getElementById("btn-themSV").addEventListener("click", function () {
  console.log("yes");
  var newSV = layThongTinTuForm();
  //   console.log("newSV: ", newSV);
  //push:Nhập thêm danh sách sinh viên vào mảng

  var isValid =
    validator.kiemTraRong(
      newSV.ma,
      "spanMaSV",
      "Mã sinh viên không được để rỗng"
    ) &&
    validator.kiemtraDoDai(
      newSV.ma,
      "spanMaSV",
      "Mã sinh viên không được lớn hơn hoặc nhỏ hơn 4 kí tự",
      4,
      4
    );
  // &&
  // validator.kiemTraId(newSV.ma, "spanMaSV", "Mã sinh viên không được trùng");
  //kiểm tra tên
  isValid &= validator.kiemTraRong(
    newSV.ten,
    "spanTenSV",
    "Tên sinh viên không được để rỗng"
  );
  //kiểm tra email
  isValid &=
    validator.kiemTraRong(
      newSV.email,
      "spanEmailSV",
      "Email không được để rỗng"
    ) &&
    validator.kiemtraEmail(
      newSV.email,
      "spanEmailSV",
      "Nhập sai email vui lòng nhập lại"
    );
  isValid &= validator.kiemTraRong(
    newSV.matKhau,
    "spanMatKhau",
    "Chưa nhập Password"
  );
  //toán
  isValid &=
    validator.kiemTraRong(
      newSV.toan,
      "spanToan",
      "Điểm Toán sinh viên không được để rỗng"
    ) &&
    validator.kiemtraDiem(
      newSV.toan,
      "spanToan",
      "Điểm toán sinh viên chỉ được từ 0 đến 10",
      0,
      10
    );
  isValid &=
    validator.kiemTraRong(
      newSV.ly,
      "spanLy",
      "Điểm Lý sinh viên không được để rỗng"
    ) &&
    validator.kiemtraDiem(
      newSV.ly,
      "spanLy",
      "Điểm Lý sinh viên chỉ được từ 0 đến 10",
      0,
      10
    );
  isValid &=
    validator.kiemTraRong(
      newSV.hoa,
      "spanHoa",
      "Điểm Hóa sinh viên không được để rỗng"
    ) &&
    validator.kiemtraDiem(
      newSV.hoa,
      "spanHoa",
      "Điểm Hóa sinh viên chỉ được từ 0 đến 10",
      0,
      10
    );

  if (isValid) {
    dssv.push(newSV);
    //tạo json dùng (stringify để băm array thành JSON để lưu vào JSON)
    var dssvJson = JSON.stringify(dssv);
    //lưu json vào localStorage
    localStorage.setItem("DSSV", dssvJson);

    renderDSSV(dssv);
    // console.log("dssv: ", dssv);
  }
});

//hàm xóa sv
function xoaSinhVien(id) {
  // console.log(id);
  //tạo biến index chứa function timkiemViTri(id,dssv)
  var index = timKiemViTri(id, dssv);

  // console.log('index: ', index);
  // nếu khác -1 là tìm thấy vị trí thì xóa
  if (index != -1) {
    //chỗ này dùng để xóa
    dssv.splice(index, 1);
    //xóa xong rồi render ra vị trí
    renderDSSV(dssv);
  }
}
//hàm sửa SinhVien
function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}
////chức năng cập nhật sinh viên
document.getElementById("btn-capNhatSv").addEventListener("click", function () {
  console.log("yes");
  //tạo biến updatedSV để lấy thông tin từ form trong hàm đã làm trước đó
  var updatedSV = layThongTinTuForm();
  console.log("updatedSV: ", updatedSV);

  var maSV = updatedSV.ma;
  console.log("maSV: ", maSV);
  var index = timKiemViTri(maSV, dssv);
  console.log("index: ", index);
  if (index != -1) {
    dssv[index] = updatedSV;
    console.log("dssv[index]: ", dssv[index]);

    var dssvJson = JSON.stringify(dssv);
    console.log("dssvJson: ", dssvJson);
    localStorage.setItem("DSSV", dssvJson);

    renderDSSV(dssv);
  }

  // document.getElementById("txtMaSV").disabled = false;
});

document.getElementById("btn-reset").addEventListener("click", function () {
  // console.log("yess");
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  // layThongTinTuForm()="";
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtMaSV").disabled = false;
});
